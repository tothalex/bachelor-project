

package examples

import java.io._ 


object fileWriteTest {

    def passToFunction(bw: BufferedWriter) = {
        bw.write("\ncross function\n");
    }

    def main (args: Array[String]): Unit = {
        val filename = "test.txt";

        // FileWriter
        val file = new File(filename);
        val bw = new BufferedWriter(new FileWriter(file));

        bw.write("something");
        bw.write("else");
        passToFunction(bw);
        bw.close();
    }
}
