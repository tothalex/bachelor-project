all: build run-local


run-local:
	# sbt run
	# I have added spark to PATH by default, this does not work if spark is not added
	# spark-submit target/scala-2.12/sparkapp_2.12-0.1.jar
	#spark-submit target/scala-2.11/benchmark_2.11-0.1.jar local[*] output.csv
	/home/alex/Documents/work/uni/research/spark/bin/spark-submit --driver-memory 16g --master=local /home/alex/Documents/work/uni/research/bachelor-project/target/scala-2.11/benchmark_2.11-0.1.jar local test.csv
build: 
	#sbt compile
	sbt package

clean:
	rm -rf target project

run-cluster:
	#make MASTER="spark://node059.ib.cluster:7077" OUTPUT_FILE="spark-8nodes.csv" run-cluster
	~/frameworks/spark-2.4.0/bin/spark-submit --master=$(MASTER) ~/spark-project/target/scala-2.11/benchmark_2.11-0.1.jar $(MASTER) $(OUTPUT_FILE)

deploy:
	#make TIME="01:00:00" NUM_MACHINES="4" deploy 
	RESERVATION_ID=`$(DEPLOYER_HOME)/deployer preserve create-reservation -q -t "$(TIME)" $(NUM_MACHINES)` ;\
 $(DEPLOYER_HOME)/deployer deploy --preserve-id $$RESERVATION_ID -s $(DEPLOYER_HOME)/env/das5-spark.settings spark 2.4.0
        
.PHONY: all run build clean test
