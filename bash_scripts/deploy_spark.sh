RESERVATION_ID=${1}
if [ -z "${RESERVATION_ID}" ] ; then
    echo "Reservation ID not given"
    exit 1
fi
${DEPLOYER_HOME}/deployer deploy --preserve-id $RESERVATION_ID -s ${DEPLOYER_HOME}/env/das5-spark.settings spark 2.4.0
