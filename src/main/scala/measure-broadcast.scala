// package Benchmark

/**
 * Created by Alexandru Toth on 19/01/2020
 * tasks were added with inspiration from https://gist.github.com/animeshtrivedi/c3cdfc5d022c38f90d75430cf5f5f397
 */

import scala.collection.mutable.ArrayBuffer
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import java.io._ 

object Benchmark {


    def createDataArray(n: Int): ArrayBuffer[Int] =  {
        var myList = new ArrayBuffer[Int](n);
        for (i <- 0 to n ) {
            myList += i;
        }
        // println("Array buffer is ", myList);
        myList;
    }



    def createBroadcastVariable(arr: ArrayBuffer[Int], n: Int, sc: SparkContext, bw: BufferedWriter) = {
        // arr.saveAsTextFile(outputDir + "_" + partition)
        //val t1 = System.nanoTime();
        val tasks = 192
        val t1 = System.currentTimeMillis();
        val broadcastVar = sc.broadcast(arr);
        val accu = sc.longAccumulator("counter")
        // broadcastVar.value maybe not needed, it accesses the value itself
        //val t2 = System.nanoTime();
        

        val t2 = System.currentTimeMillis();

        val triggetComputer = sc.parallelize(1 to tasks, tasks).map(task => {
            // for each partition we need to read all broadcast variable
            var elSum = 0L
            val bValue = broadcastVar.value
            for ( i <- 100 to 200) {
            // sum up their length
                elSum+=bValue(i) 
            }
            (accu.add(elSum))
        }).count

        val t3 = System.currentTimeMillis();
        broadcastVar.destroy(); // destroy the variable, forcing the app to create a new one
        val diff1 = (t2 - t1); // create broadcast variable
        val diff2 = (t3 - t2); // parallel operation
        val diff3 = (t3 - t1); // total
        println("It took " + diff3 + " milliseconds to broadcast the variable of size " + n + " first part: " + diff1 + " second part: " + diff2 );
        bw.write(n + "," + diff1 + "," + diff2 + "," + diff3 + "," + accu.value + "\n");
	    bw.flush();
        accu.reset()
    }
    
    def main (args: Array[String]): Unit = {
        var master="";
        var filename = "";
        println("your args were: ")
        args.foreach( arg => print(arg + " "))
        println()
        if (args.length != 2) {
            println("not enough arguments!")
            println("please provide the master and the name of the output file")
            System.exit(0)
        } else {
            master = args(0);
            filename = args(1);
        }


        // FileWriter
        val file = new File(filename);
        val bw = new BufferedWriter(new FileWriter(file));
        bw.write("array size, create broadcast var time, parallel op time, total time, sum value\n");

        val conf = new SparkConf()
        .setMaster(master) 
        .setAppName("Benchmark")
        val sc = new SparkContext(conf)

        // range is 1 to 250 000 000
        //val range = Array(1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 250000000);
        //val range = Array(1000, 10000, 100000, 1000000, 10000000, 100000000); 
        val range = Array(1000, 10000, 100000, 1000000, 10000000, 100000000); 
        for (value <- range) {
	    val array = createDataArray(value);
            for( w <- 0 to 9) {
                createBroadcastVariable(array, value, sc, bw);
            }
        }
        bw.close()
    }
}
