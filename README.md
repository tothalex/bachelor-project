repo containing the application for my bachelor thesis

thanks to Alex Uta for coordination

scripts to run spark on das5 borrowed from Tim Hegeman `https://github.com/thegeman/das-bigdata-deployment.git`

idea behind the application

1. measure time spent in broadcast and how well it behaves on different load sizes

10 lines
100
1k
10k
100k
500k
1M
10M
100M
1G 
10G 10 might be too much for a broadcast operation  

data taken from /var/scratch/aua400

2. implement other broadcast algorithms taken from my paper



BUILD:

In order to build the app use `sbt package` this will create the jar file in the the target directory, currently with scala-2.11, `build.sbt` has more details


https://www2.cs.duke.edu/courses/fall15/compsci290.1/TA_Material/jungkang/how_to_run_spark_app.pdf
RUN:

to run locally, use: `spark-submit ~/Documents/work/uni/Research/bachelor-project/target/scala-2.12/sparkapp_2.12-0.1.jar`

to run on the cluster, use: `frameworks/spark-2.4.0/bin/spark-submit --master=spark://node059.ib.cluster:7077 spark-project/target/scala-2.11/sparkapp_2.11-0.1.jar spark://node059.im.cluster:7077 spark-8nodes.csv`

For now running under the spark-shell, fix to get spark-submit to work, problem with compilation inside the cluster

`$DEPLOYER_HOME/deployer preserve create-reservation -q -t "00:15:00" 4`

`$DEPLOYER_HOME/deployer deploy --preserve-id $RESERVATION_ID -s $DEPLOYER_HOME/env/das5-spark.settings spark 2.4.0`

To connect to Spark using a shell, first connect to the application master via SSH, then run `$DEPLOYER_HOME/frameworks/spark-2.4.0/bin/spark-shell`

to kill reservation use `$DEPLOYER_HOME/deployer preserve kill-reservation $RESERVATION_ID`

to list reservations use `$DEPLOYER_HOME/deployer preserve list-reservations`

to fetch reservations use `$DEPLOYER_HOME/deployer fetch-reservation $RESERVATION_ID`


4 nodes - 100k  151355509 nanoseconds 
          1 mil 614387360nanoseconds

benchmark results 3 runs, das5, 4 machines (1 master 3 nodes)

steps to reach test: :load /home/aan285/spark-project/src/main/scala/measure-broadcast.scala

sbt can be added to das5 

average of 5 runs
10   : 8 ms 
100  : 7 ms
1k   : 10.8 ms
10k  : 13.2 ms
100k : 65.6 ms
1mil : 507.4 ms
5mil : 3388,8 ms
6mil : 4558.4 ms
7mil : 4803 ms 
10mil: 8751.8 ms
20mil: 17175.6 ms

observations: high variability at higer amounts of data, data not usable until more testing is done    



where should I look for improvements? low amounts of data or high amounts(assume by default high)
1k -> 1GB  256 mil entries
What is a good amount of runs for the value to be valid?
10.
noticed variability, what to do about that?
how to run it better?

ip-urile 141 e ethernet si 149 infiniband

ib e infiniband

ethernet e cm

test pe das pe cm pe ethernet

search /etc/hosts to see cm 

TODO: scripturi pentru spark
 do the same test with 8 nodes and 16 nodes 32 nodes
 create script to make data into csv

 create script to make csv into averages

 test broadcast with a benchmark with join ( forced broadcast hash join)
 ca sa vedem daca se aplica fix 
