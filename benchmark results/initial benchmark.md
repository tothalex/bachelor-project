benchmark results 3 runs, das5, 4 machines (1 master 3 nodes)

steps to reach test: :load /home/aan285/spark-project/src/main/scala/measure-broadcast.scala


10 : 8
100: 
1k:
10k
100k
1mil
5mil
10mil 



scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(10), sc)
It took 6 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(10), sc)
It took 3 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(10), sc)
It took 4 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(10), sc)
It took 3 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(10), sc)
It took 24 milliseconds to broadcast the variable

10: 8

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(100), sc)
It took 8 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(100), sc)
It took 3 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(100), sc)
It took 5 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(100), sc)
It took 4 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(100), sc)
It took 3 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(100), sc)
It took 12 milliseconds to broadcast the variable

100: 7



scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(1000), sc)
It took 36 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(1000), sc)
It took 6 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(1000), sc)
It took 6 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(1000), sc)
It took 3 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(1000), sc)
It took 3 milliseconds to broadcast the variable

1000: 10.8

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(10000), sc)
It took 12 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(10000), sc)
It took 27 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(10000), sc)
It took 11 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(10000), sc)
It took 9 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(10000), sc)
It took 7 milliseconds to broadcast the variable

10000:  13.2


scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(100000), sc)
It took 63 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(100000), sc)
It took 46 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(100000), sc)
It took 60 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(100000), sc)
It took 100 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(100000), sc)
It took 59 milliseconds to broadcast the variable

100000: 65,6


scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(1000000), sc) 
It took 490 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(1000000), sc) 
It took 603 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(1000000), sc) 
It took 384 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(1000000), sc) 
It took 619 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(1000000), sc) 
It took 441 milliseconds to broadcast the variable

1000000: 507,4


scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(5000000), sc) 
It took 2974 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(5000000), sc) 
It took 3468 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(5000000), sc) 
It took 3207 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(5000000), sc) 
It took 3624 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(5000000), sc) 
It took 3671 milliseconds to broadcast the variable

5000000:3388,8



scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(6000000), sc) 
It took 6536 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(6000000), sc) 
It took 3343 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(6000000), sc) 
It took 4422 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(6000000), sc) 
It took 4485 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(6000000), sc) 
It took 4006 milliseconds to broadcast the variable

6000000: 4558.4

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(7000000), sc) 
It took 4646 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(7000000), sc) 
It took 4438 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(7000000), sc) 
It took 5060 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(7000000), sc) 
It took 4720 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(7000000), sc) 
It took 5151 milliseconds to broadcast the variable

7000000: 4803


scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(10000000), sc)
It took 8311 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(10000000), sc)
It took 9181 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(10000000), sc)
It took 12749 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(10000000), sc)
It took 7061 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(10000000), sc)
It took 6457 milliseconds to broadcast the variable

10000000:8751,8

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(20000000), sc)
It took 15606 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(20000000), sc)
It took 15148 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(20000000), sc)
It took 26447 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(20000000), sc)
It took 14673 milliseconds to broadcast the variable

scala> Benchmark.createBroadcastVariable(Benchmark.createDataArray(20000000), sc)
It took 14004 milliseconds to broadcast the variable

20000000: 17175,6
